const {
  chat: { sanitize },
} = OMEGGA_UTIL;

const colorKey = (color, str) => `"<color=\\"${color}\\">${sanitize(str + "")}</>"`;
const yellow = (str) => colorKey("ffff00", str);
const red = (str) => colorKey("ff5555", str);
const green = (str) => colorKey("55ff55", str);
const grey = (str) => colorKey("aaaaaa", str);

module.exports = class DiceRoller {
  constructor(omegga, config, store) {
    this.omegga = omegga;
    this.config = config;
    this.store = store;
  }

  async message(msg, who) {
    if ( who === undefined || who === '' || who === null ) {
      Omegga.broadcast(msg);
    } else {
      Omegga.whisper(who, msg);
    }
  }

  async roll(who, type, ...args) {
    if ( this.config["enabled"] !== true ) {
      return;
    }
    else if ( this.config['host-only'] && !Omegga.getPlayer(who).isHost() ) {
      this.message(`${red("Rolling dice is currently host-only.")}`, who);
      return;
    }

    if ( this.config['banned'].split(',').includes(who) ) {
      this.message(red("You have been bannd from rolling dice."), who);
      return;
    }

    let arg;
    for (let curr = 0; curr < args.length; curr++) {
      arg = args[curr];
      let i;
      let d,
        num,
        face = null;

      const len = arg.length;
      if ( len === 0 ) continue;

      if ( arg.includes('d') === false ) {
        this.message(red("You have not entered any dice to roll."), who);
        continue;
      }
      // Parse text to find what dice are being rolled
      for (i = 0; i < len; i++) {
        d = arg.substring(i, i + 1);
        if (d === "d") {
          face = arg.substring(i + 1, len);
          num = arg.substring(0, i);
          break;
        } else {
          num = arg.substring(0, i + 1);
        }
      }

      if ( typeof num !== "number" && isFinite(num) && num.length === 0) {
        num = 1;
      } else {
        num = Math.ceil(num);
        if (num > this.config["max-dice"]) {
          this.message(red(`Limit number of rolls to ${this.config["max-dice"]}.`), who);
          continue;
        } else if ( num < 1 ) {
          this.message(red("What's the point in rolling nothing?"), who);
          continue;
        }
      }
      if (face === null || face < 2) {
        this.message(red("Please enter a dice face to roll."), who);
        continue;
      } else if (face > this.config["max-face"]) {
        this.message(`Limit dice face size to ${this.config["max-face"]}.`, who);
        return;
      } else {
        face = Math.floor(face);
      }

      let all_rolls = "";
      let total = 0;

      if ( num > 1 ) {
        this.message("<b>Rolled</b>:", type);
      }
      for (i = 0; i < num; i++) {
        let roll = Math.floor(Math.random() * face + 1);
        const tmp = roll + "";
        if (tmp === "NaN") {
          this.message(`${red("Cannot roll that.")}`, who);
        } else {
          let display = roll + "";
          if (roll === 1) {
            display = red(display);
          } else if (roll + "" === face + "") {
            display = green(display);
          }
          if (num > 1) {
            if (all_rolls === "") {
              all_rolls = '';
            } else if ( all_rolls.length > 200 ) {
              this.message(all_rolls, type);
              all_rolls = '';
            }
            all_rolls += `${grey("[")}${display} / ${yellow(face + "")}${grey("]")}`;
            total += roll;
            if (i+1 < num) {
              all_rolls += " + ";
            }
          } else {
            this.message(`<b>Rolled</b>: ${display} / ${yellow(face + "")}`, type);
          }
        }
      }

      if (num > 1) {
        all_rolls += `= ${yellow(total + "")}`;
        this.message(all_rolls, type);
      }
    }
  }

  async init() {
    Omegga.on("chatcmd:roll", (name, ...args) => {
      this.roll(name, null, ...args);
    });
    // Omegga.on("cmd:roll", (name, ...args) => {
    //   this.roll(name, null, ...args);
    // });

    Omegga.on("chatcmd:proll", (name, ...args) => {
      this.roll(name, name, ...args);
    });
    Omegga.on("cmd:proll", (name, ...args) => {
      this.roll(name, name, ...args);
    });
  }

  async stop() {}
};
