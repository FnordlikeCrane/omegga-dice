# Dice Roller

A dice rolling plugin for [omegga](https://github.com/brickadia-community/omegga).

## Commands

* `!roll [amount]<face>` - rolls dice for everyone to see
* `!proll [amount]<face>` - rolls dice privately

Where `<face>` is a number proceeded by the letter `d`.


Example: `!roll 8d6 1d4 d12`


## Configs

* `enabled` - Dice rolling is enabled
* `host-only` - Toggles host only mode
* `banned` - Comma separated string of players who may not use dice rolling commands
* `max-dice` - Maximum amount of dice to roll per input allowed
* `max-face` - Highest dice face value allowed to be rolled
